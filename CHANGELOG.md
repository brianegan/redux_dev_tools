# Changelog

## 0.1.1

  - Update README

## 0.1.0

  - Initial version. Allows you to create a `DevToolsStore` during dev mode, which will allow you to time travel through the states of your application. 
